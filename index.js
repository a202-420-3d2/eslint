var x = 5+2;
var z = [1,4,6, 9
];
var o;
var age = 19;
o ={nom:"Louis",age: age}

function f1() {
  return x+7
}

function f2() {
  return o['nom'];
}

function f3(a,b,c) {
  return a*b;
}

function f4(arr) {
  var sum = 0;
  for(var i=0; i<arr.length;i++) {
    sum = sum+arr[i];
  }
  return sum;
}
/* utilisez ces appels pour vérifier que votre code n'est pas dénaturé */
console.log(`${x} === 7`);
console.log(`${f1()} === 14`);
console.log(`${f2()} === Louis`);
console.log(`${f3(2, 3)} === 6`);
console.log(`${f3(4, 5)} === 20`);
console.log(`${f4(z)} === 20`);
